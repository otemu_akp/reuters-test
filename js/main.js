(function(ng){
	ng.module('app', [])
	.directive('toggleComponent', function() {
  	var controller = ['$scope', function ($scope) {
  		$scope.isActive = false;
	  	$scope.activeButton = function() {
	    	$scope.isActive = !$scope.isActive;
	  	}     
	  	$scope.closeButton = function() {
	    	$scope.$parent.showModal = false;
	  	}         
    }]      
	  return {
	    templateUrl: 'js/module.directive.html',
	    controller: controller,
	  };
	});

})(angular);